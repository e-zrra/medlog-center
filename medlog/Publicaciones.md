# PUBLICACIONES

* Auth
  http://mherman.org/blog/2017/01/05/token-based-authentication-with-angular/#.WHUZefl96M9

* Seguridad
  https://www.linkedin.com/groups/2906459/2906459-6202855450912919556?midToken=AQHKm6jk9vvufw&trk=eml-b2_anet_digest_of_digests-hero-11-discussion~subject&trkEmail=eml-b2_anet_digest_of_digests-hero-11-discussion~subject-null-3nnvg2~ixnjfdun~o

* Promesas
  https://developers.google.com/web/fundamentals/getting-started/primers/promises

* Async y await
  https://www.twilio.com/blog/2015/10/asyncawait-the-hero-javascript-deserved.html

* Mejores practicas de mean
  https://github.com/KimSarabia/mean-scaffold

* Mejores practicas de JS
  https://github.com/airbnb/javascript

* Publicacion de Interceptor Angular
  https://thinkster.io/interceptors
