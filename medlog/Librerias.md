# LIBRERIAS

- Helmet

- PM2 - Ensure your app automatically restarts - $ npm install pm2 -g

- Repositorio mean jwt rest auth
  https://github.com/sri7vasu/mean-jwt-rest-auth/blob/master/app/js/app.js

- Repositorio mean auth
  https://github.com/simonholmes/MEAN-stack-authentication/blob/master/app.js

- Repositorio mean registration login example
  https://github.com/cornflourblue/mean-stack-registration-login-example

- Repositorio Angular Drywall
  https://github.com/arthurkao/angular-drywall

- Libreria de notificaciones para panel
  https://github.com/matowens/ng-notify

- Rutas con Angular
  http://angular-ui.github.io/
