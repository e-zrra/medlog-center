# RECURSOS

* Imagenes
  https://pixabay.com/en/doctor-dentist-dental-clinic-1149149/
  https://pixabay.com/en/computer-business-typing-keyboard-1149148/

* Tema de website
  https://dribbble.com/shots/2383294-4Doctors-Landing/attachments/458766
  https://www.yelp.es/signup

* Tema de panel
  http://uikreative.com/

* Tema de panel layout
  https://dribbble.com/shots/1315388-Dashboard-Web-App-Product-UI-Design-Job-Summary/attachments/184703

* Front end exercise
  https://github.com/JoanClaret/frontend-exercise

* JS Boilerplate
  https://github.com/JoanClaret/js-boilerplate

* Files and Images
  http://jsfiddle.net/danialfarid/2vq88rfs/136/
  http://jsfiddle.net/danialfarid/xxo3sk41/590/
