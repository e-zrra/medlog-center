let express 	= require('express');
let	favicon 		= require('serve-favicon');
let	logger 			= require('morgan');
let	path 				= require('path');
let	cookieParser = require('cookie-parser');
let	bodyParser 	= require('body-parser');
let	config			= require('./server/config/app');
let	app 				= express();

// DB
require('./server/db/connect')

// Models
require('./server/models/city');
require('./server/models/account');
require('./server/models/consultation');
require('./server/models/country');
require('./server/models/patient');
require('./server/models/state');
require('./server/models/user');
require('./server/models/agenda');

var passport = require('passport');

// Passport
require('./server/config/passport');

// Routes
let website = require('./server/routes/website');
let patients 	= require('./server/routes/patients');
let consultations = require('./server/routes/consultations');
let countries = require('./server/routes/countries');
let profile = require('./server/routes/profile');
let users = require('./server/routes/users');
let agenda = require('./server/routes/agenda');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());

var middlewareAuthenticate = require('./server/middlewares/authenticate');

app.use('/', website);
app.use(middlewareAuthenticate);
app.use('/api', [patients, consultations, countries, profile, users, agenda]);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

app.use(function (err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401);
    res.json({"message" : err.name + ": " + err.message});
  }
});

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;
