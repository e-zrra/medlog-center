(function () {
'use strict';
	angular
		.module('app')
		.directive('showAndHideSection', showAndHideSection);

	function showAndHideSection () {
		return {
			restrict: 'E',
			scope: {
				section: "="
			},
			templateUrl: 'app/views/directives/show-and-hide-section.html'
		}
	}

})()
