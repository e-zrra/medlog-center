var mongoose = require( 'mongoose' );
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var config	= require('../config/app');
var Schema 	= mongoose.Schema;

var UserSchema   = new mongoose.Schema({
	firstName: String,
	lastName: String,
	fullName: String,
	username: { type: String, lowercase: true, unique: true, required: true },
	email: {
    type: String,
  },
	birthday: Date,
	role: String,
	createdAt: { type: Date, default: Date.now },
  updatedAt: Date,
	hash: String,
  salt: String,
	professionalData: {
		specialty: String,
		professionalLicense : String,
	},
	position: String,
	account: { type: Schema.Types.ObjectId, ref: 'Account' },
	user: { type: Schema.Types.ObjectId, ref: 'User' },
	files: [
		{ name: String, path: String }
	]
});

UserSchema.methods.setPassword = function(password){
  this.salt = crypto.randomBytes(16).toString('hex');
  this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64,'sha1').toString('hex');
};

UserSchema.methods.getSalt = function () {
	return crypto.randomBytes(16).toString('hex');
}

UserSchema.methods.getHash = function (password, salt) {
  return crypto.pbkdf2Sync(password, salt, 1000, 64,'sha1').toString('hex');
}

UserSchema.methods.validPassword = function(password) {
  var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64,'sha1').toString('hex');
  return this.hash === hash;
};

UserSchema.methods.setUser = function (user) {
	this.account = user.account;
	this.user = user.id;
};

UserSchema.methods.generateJwt = function() {
  var expiry = new Date();
  expiry.setDate(expiry.getDate() + 7);

  return jwt.sign({
    user: this._id,
		fullName: this.fullName,
		username: this.username,
		account: this.account,
		role: this.role,
    exp: parseInt(expiry.getTime() / 1000),
  }, config.secret);
};

UserSchema.methods.getFullName = function (firstName, lastName) {
	return `${firstName} ${lastName}`;
}

UserSchema.pre('save', function(next){
  this.fullName = `${this.firstName} ${this.lastName}`;
  next();
});

UserSchema.pre('update', function(next) {
		this.update({}, { $set: { updatedAt: new Date() } })
		next();
});

module.exports = mongoose.model('User', UserSchema);
