var mongoose 	= require('mongoose'),
	Schema 		= mongoose.Schema;

var CountrySchema   = new mongoose.Schema({
	name: String,
	states: [{
		type: Schema.Types.ObjectId, ref: 'State'
	}]
});

module.exports = mongoose.model('Country', CountrySchema);