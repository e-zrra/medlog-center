var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var ConsultationSchema = new mongoose.Schema({
	title: String,
	createdAt: { type: Date, default: Date.now },
  updatedAt: Date,
	patient: { type: Schema.Types.ObjectId, ref: 'Patient' },
	account: { type: Schema.Types.ObjectId, ref: 'Account' },
	user: { type: Schema.Types.ObjectId, ref: 'User' },
	vitalSigns: {
		height: {
			value: Number,
			measure: String
		},
		weight: {
			value: Number,
			measure: String
		},
		bodyMass: {
			value: Number,
			measure: String
		},
		temperature: {
			value: Number,
			measure: String
		},
		breathingFrequency: {
			value: Number,
			measure: String
		},
		bloodPressure: {
			value: Number,
			measure: String
		},
		pulse: {
			value: Number,
			measure: String
		},
		heartRate: {
			value: Number,
			measure: String
		}
	},
	physicalSystems: {
		digestive: Boolean,
		digestiveText: String,
		urinary: Boolean,
		urinaryText: String,
		lymphatic: Boolean,
		lymphaticText: String,
		dermatological: Boolean,
		dermatologicalText: String,
		neurological: Boolean,
		neurologicalText: String,
		cardiovascular: Boolean,
		cardiovascularText: String,
		osteoarticular: Boolean,
		osteoarticularText: String,
		otolaryngology: Boolean,
		otolaryngologyText: String,
		respiratory: Boolean,
		respiratoryText: String,
		psychiatricAndPsychological: Boolean,
		psychiatricAndPsychologicalText: String,
		reproductive: Boolean,
		reproductiveText: String,
		others: Boolean,
		othersText: String
	},
	reasonForVisit: String,
	subjectiveSymptoms: String,
	physicalExploration: String,
	medicalInstructions: String,
	diagnostics: String,
	medicalStudies: String,
	comments: String,
	medicines: [{
		name: String,
		dose: String,
		duration: String,
		comments: String
	}]
});
/*
date
patient_id
user_id
Diagnosis - Diagnostico
Treatment - Tratamiento

- Notes Suffering - Notas de Padecimiento
Reason for Visit - Razón de la visita
Subjective symptoms - Síntomas Subjetivos
Physical exploration - Exploración Física

- Vital signs - Signos Vitales
Height - Altura (mts)
Weight - Peso (kg)
Body mass - Masa Corporal (bmi)
Temperature - Temperatura (C)
Breathing frequency - Frecuencia Respiratoria (r/m)
Blood pressure - Presión Arterial (mmHG)
Heart rate - Frecuencia Cardiaca (fc)

- Physical exam - examen físico
*/

ConsultationSchema.methods.setUser = function (user) {
	this.account = user.account;
	this.user = user.id;
};

ConsultationSchema.pre('update', function(next) {
	this.update({}, { $set: { updatedAt: new Date() } })
	next();
});

module.exports = mongoose.model('Consultation', ConsultationSchema);
