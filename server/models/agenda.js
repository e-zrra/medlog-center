var mongoose 	= require('mongoose'),
	Schema 		= mongoose.Schema;

var AgendaSchema = new mongoose.Schema({
	reason: String,
  date: Date,
  startTime: String,
  endTime: String,
	createdAt: { type: Date, default: Date.now },
  updatedAt: Date,
	patient: { type: Schema.Types.ObjectId, ref: 'Patient' },
	account: { type: Schema.Types.ObjectId, ref: 'Account' },
	user: { type: Schema.Types.ObjectId, ref: 'User' },
});

AgendaSchema.methods.setUser = function (user) {
	this.account = user.account;
	this.user = user.id;
};

AgendaSchema.pre('update', function(next) {
	this.update({}, { $set: { updatedAt: new Date() } })
	next();
});

module.exports = mongoose.model('Agenda', AgendaSchema);
