var mongoose 	= require('mongoose'),
	Schema 		= mongoose.Schema;

var PatientSchema   = new mongoose.Schema({
	firstName: String,
	lastName: String,
	fullName: String,
	email: String,
	birthday: Date,
	gender: String,
	uprc: String,
	address: String,
	country: String,
	state: String,
	city: String,
	createdAt: { type: Date, default: Date.now },
  updatedAt: Date,
	account: { type: Schema.Types.ObjectId, ref: 'Account' },
	user: { type: Schema.Types.ObjectId, ref: 'User' },
	phone: String,
	mobilePhone: String,
	zipcode: Number,
	pathologicBackground: {
		hospitalizationPreviousLogical: Boolean,
		hospitalizationPreviousText: String,
		surgeriesPreviousLogical: Boolean,
		surgeriesPreviousText: String,
		diabetesLogical: Boolean,
		diabetesText: String,
		thyroidDiseasLogical: Boolean,
		thyroidDiseasText: String,
		arterialHypertensionLogical: Boolean,
		arterialHypertensionText: String,
		heartDiseaseLogical: Boolean,
		heartDiseaseText: String,
		injuriesLogical: Boolean,
		injuriesText: String,
		cancerLogical: Boolean,
		cancerText: String,
		tuberculosisLogical: Boolean,
		tuberculosisText: String,
		transfusionsLogical: Boolean,
		transfusionsText: String,
		othersLogical: Boolean,
		othersText: String
	},
	familyHereditaryBackground: {
		diabetesLogical: Boolean,
		diabetesText: String,
		heartDiseaseLogical: Boolean,
		heartDiseaseText: String,
		arterialHypertensionLogical: Boolean,
		arterialHypertensionText: String,
		thyroidDiseasLogical: Boolean,
		thyroidDiseasText: String,
		othersLogical: Boolean,
		othersText: String
	},
	obstetricGynecologicBackground: {
		heartDiseaseLogical: Boolean,
		heartDiseaseText: String,
		asthmaAllergiesLogical: Boolean,
		asthmaAllergiesText: String,
		tuberculosisLogical: Boolean,
		tuberculosisText: String,
		hypertensionLogical: Boolean,
		hypertensionText: String,
		dateFirstMenstruationLogical: Boolean,
		dateFirstMenstruationText: String,
		dateLastMenstruationLogical: Boolean,
		dateLastMenstruationText: String,
		menstruationFeaturesLogical: Boolean,
		menstruationFeaturesText: String,
		pregnanciesLogical: Boolean,
		pregnanciesText: String,
		tumoursLogical: Boolean,
		tumoursText: String,
		uterineCancerLogical: Boolean,
		uterineCancerText: String,
		breastCancerLogical: Boolean,
		breastCancerText: String,
		othersLogical: Boolean,
		othersText: String
	},
	noPathologicBackground: {
		seizuresLogical: Boolean,
		SeizuresText: String,
		physicalActivityLogical: Boolean,
		physicalActivityText: String,
		smokingLogical: Boolean,
		smokingText: String,
		alcoholismLogical: Boolean,
		alcoholismText: String,
		useOtherSubstancesLogical: Boolean,
		useOtherSubstancesText: String,
		recentVaccineOrImmunizationLogical: Boolean,
		recentVaccineOrImmunizationText: String,
		othersLogical: Boolean,
		othersText: String
	},
	allergies: String,
	notes: String,
	files: [{
		name: String, filename: String, path: String, mimetype: String, destination: String, createdAt: { type: Date, default: Date.now },
	}]

	/*
		Medicamentos Activos - Arreglo
		Medicamentos Activos - Coleccion
		Files - Arreglo
	*/

});

PatientSchema.pre('save', function(next) {
  this.fullName = `${this.firstName} ${this.lastName}`;
  next();
});

PatientSchema.pre('update', function (next) {
	this.update({}, { $set: { updatedAt: new Date() } })
	next();
});

PatientSchema.methods.setUser = function (user) {
	this.account = user.account;
	this.user = user.id;
};

PatientSchema.methods.getFullName = function (firstName, lastName) {
	return `${firstName} ${lastName}`;
}

module.exports = mongoose.model('Patient', PatientSchema);
