var mongoose 	= require('mongoose'),
	Schema 		= mongoose.Schema;

var CitySchema   = new mongoose.Schema({
	name: String,
});

module.exports = mongoose.model('City', CitySchema);