var mongoose = require( 'mongoose' );
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var Schema 		= mongoose.Schema;

var AccountSchema   = new mongoose.Schema({
  companyName: String,
	createdAt: { type: Date, default: Date.now },
  updatedAt: Date,
});

AccountSchema.pre('update', function(next) {
	this.update({}, { $set: { updatedAt: new Date() } })
	next();
});

module.exports = mongoose.model('Account', AccountSchema);
