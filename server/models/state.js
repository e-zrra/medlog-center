var mongoose 	= require('mongoose'),
	Schema 		= mongoose.Schema;

var StateSchema   = new mongoose.Schema({
	name: String,
	cities: [{
		type: Schema.Types.ObjectId, ref: 'City'
	}]
});

module.exports = mongoose.model('State', StateSchema);
