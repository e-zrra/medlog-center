module.exports = {
	"port": process.env.PORT || 3001,
	"database": "localhost:27017/test",
	"secret": "medlog-center-application"
};
