var express = require('express');
var mongoose = require('mongoose');
var Country = mongoose.model('Country');

var router = express.Router();

router.route('/countries')
		.get(function (req, res) {

			Country.find(function(err, countries) {

				if (err) res.send(err);

				res.json(countries);
			}).sort({ name: 1 });

		})

module.exports = router;
