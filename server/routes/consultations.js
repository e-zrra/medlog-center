var express = require('express');
var consultations = require('../controllers/consultations');

var router = express.Router();

router.route('/consultations')
			.post(consultations.create)

router.route('/consultations/:id')
			.get(consultations.get)
			.put(consultations.update)
			.delete(consultations.delete)

router.route('/consultations/patient/:patientId')
			.get(consultations.patient);

module.exports = router;
