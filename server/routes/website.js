var express = require('express');
var router = express.Router();
var jwt = require('express-jwt');
var config	= require('../config/app');
var auth  = require('../controllers/authentication');
var mongoose = require('mongoose');
var User = mongoose.model('User');

// var authJWT = jwt({
//   secret: config.secret,
//   userProperty: 'payload'
// });


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Home' });
});

router.get('/panel', function(req, res, next) {
  res.render('panel');
});

router.get('/login', function(req, res, next) {
  res.render('login', { title: 'Login' })
});

router.post('/login', auth.login);

router.get('/register', function(req, res, next) {
  res.render('register', { title: 'Register' })
});

router.post('/register', auth.register);

router.get('/test', function (req, res) {
  User.find({}, function (err, users) {
		if (err) {
      res.json({ test: true })
    }
		res.json(users);
	});
  // res.json({ test: true })
})

module.exports = router;
