var express = require('express');
var agenda = require('../controllers/agenda');
var router = express.Router();

router.route('/agenda')
		.get(agenda.index)
		.post(agenda.create)

router.route('/agenda/:id')
		.get(agenda.get)
		.put(agenda.update)
		.delete(agenda.delete)

module.exports = router;
