var express = require('express');
var profile = require('../controllers/profile');

var router = express.Router();

router.route('/profile')
		.get(profile.get)
		.put(profile.update)

module.exports = router;
