var express = require('express');
var patients = require('../controllers/patients');
var router = express.Router();

router.route('/patients')
		.get(patients.index)
		.post(patients.create)

router.route('/patients/:id')
		.get(patients.get)
		.put(patients.update)
		.delete(patients.delete)

router.get('/patients/:id/agenda', patients.agenda);
router.get('/patients-all/', patients.all);
router.get('/patients-search/', patients.search);
router.put('/patients/:id/medical-history', patients.medicalHistoryUpdate);
router.post('/patients/:id/files/create', patients.fileCreate);
router.delete('/patients/:patientId/files/:id', patients.fileDestroy);

module.exports = router;
