var express = require('express');
var users = require('../controllers/users');

var router = express.Router();

router.route('/users')
		.get(users.index)
		.post(users.create)

router.route('/users/:id')
		.get(users.get)
		.put(users.update)
		.delete(users.delete)

module.exports = router;
