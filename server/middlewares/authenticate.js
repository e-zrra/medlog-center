var jwt     = require('jsonwebtoken');
var config	= require('../config/app');
var mongoose 	= require('mongoose');
var User 	= mongoose.model('User');

module.exports = function(req, res, next) {

  if(req.headers.authorization) {
    var authorization = req.headers.authorization, decoded;

    jwt.verify(authorization.split(' ')[1], config.secret, function(err, decoded) {

      // Error handling
      if(err) return res.status(401).send('Unauthorized');

      // Valid if user is deleted
      if (decoded.user) {
        User.findById(decoded.user, function (err, user) {
          if (err || !user) {
            return res.status(401).send('Unauthorized');
          }

          let userData = {
            id: user._id,
            username: user.username,
            fullName: user.fullName,
            account: user.account
          }
          req.user = userData;
          return next();
        })
      }

      // return res.status(403).send('Access to the requested resource has been denied');
      // next();
    });
  } else {
    // Error 403: Access forbidden
    return res.status(401).send("Unauthorized");
  }
};
