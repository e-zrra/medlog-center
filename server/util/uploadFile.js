var mkdirp = require('mkdirp');
var multer  = require('multer');

exports.upload =  function (destination) {
  var directory = `./public/${destination}`;
  var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      mkdirp(directory, function (err) {
        if (err) console.error(err)
        cb(null, directory)
      });
    },
    filename: function (req, file, cb) {
      cb(null, `${Date.now()}_${file.originalname}`)
    }
  });

  return multer({ storage: storage }).single('file');
}
