var mongoose 	= require('mongoose'),
  config			= require('../config/app');

mongoose.connect(config.database, function (err) {
  mongoose.Promise = global.Promise;
	if (err) { console.error('ERROR: ' + err); throw err; }
	console.info("Connected to MongoDB")
});

mongoose.connection.on('error', function(err) {
  console.log('Mongoose connection error: ' + err);
});

mongoose.connection.on('disconnected', function() {
  console.log('Mongoose disconnected');
});
