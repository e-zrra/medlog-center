var mongoose = require('mongoose');
var Consultation = mongoose.model('Consultation');

exports.create = function (req, res, next) {
  var consultation = new Consultation(req.body);
  consultation.setUser(req.user);
  consultation.save(function (err, data) {
    if (err) return next(err);
    res.json({ success: true, data: data });
  });
}

exports.get = function (req, res, next) {
  Consultation.findById(req.params.id).populate('patient').exec(function (err, consultation) {
    if (err) return next(err);
    res.json(consultation);
  })
}

exports.update = function (req, res, next) {
  Consultation.findById(req.params.id, function (err, consultation) {
    if (err) return next(err);
    consultation.update(req.body, function (err) {
      if (err) return next(err);
      res.json({ message: 'Updated' });
    });
  });
}

/**
 * Delete
 */
exports.delete = function (req, res, next) {
	Consultation.findByIdAndRemove(req.params.id, function(err) {
    if (err) { return next(err)};
    res.json("deleted");
  });
};

exports.patient = function (req, res, next) {

  var page = req.query.page ? req.query.page : 1;
  var per_page = 10;
  var total = 0;
  var filter = { 'patient': req.params.patientId };

  Consultation.find(filter, function (err, consultations) {
    if (err) return next(err);
    Consultation.count(filter, function (err, count) {
      if (err) return next(err);
      res.json({ data: consultations, count: count });
    });
  }).sort('-createdAt').skip((page-1)*per_page).limit(per_page);

}
