var passport = require('passport');
var mongoose = require('mongoose');
var User = mongoose.model('User');
var Account = mongoose.model('Account');
var passport = require('passport');
var $q = require('q');

let ROLE_MASTER = 'master';

module.exports.register = function(req, res, next) {

  if(!req.body.firstName || !req.body.username || !req.body.password) {
		return res.status(400).json({ message: 'Please fill out all fields.'});
	}

	if(req.body.password  !== req.body.confirmPassword) {
		return res.status(400).json({ message:"Passwords do not match. Please enter your password again." , type : "danger" });
	}

  var result = $q.defer()

	User.find({ username: req.body.username }).exec(function(err, users) {
		if(users.length > 0){
      result.reject({ message:"This username already exist. Please enter another username." , type : "danger" })
      // return res.status(400).json({ message: 'This username already exist. Please enter another username.'});
		} else {
      result.resolve()
    }
  });

  result.promise.then(function () {

    var user = new User();
    var account = new Account();

    user.firstName = req.body.firstName;
    user.lastName = req.body.lastName;
    user.username = req.body.username;
    user.role = ROLE_MASTER;
    user.setPassword(req.body.password);
    account.companyName = req.body.company_name;

    account.save(function (err) {
      if (err) return res.status(400).json({ message: 'Error, with the server. Saving account'});

      user.account =  account._id;

      user.save(function(err) {
        if (err) return res.status(400).json({ message: 'Error, with the server. Saving user'});
        account.save(function (err) {
          if (err) return res.status(400).json({ message: 'Error, with the server. Save user in account'});

          let token = user.generateJwt();
          res.status(200);
          res.json({
            "token" : token
          });
        });
      });
    })

  }).fail(function (err) {
    return res.status(400).json(err);
  })

};

module.exports.login = function(req, res) {

  if(!req.body.username || !req.body.password) {
		return res.status(400).json({ message: 'Please fill out all fields'});
	}

  passport.authenticate('local', function(err, user, info){

    console.log(user)
    var token;

    // If Passport throws/catches an error
    if (err) {
      res.status(404).json(err);
      return;
    }

    // If a user is found
    if(user){
      token = user.generateJwt();
      res.status(200);
      res.json({
        "token" : token
      });
    } else {
      // If user is not found
      res.status(401).json(info);
    }
  })(req, res);

};
