var mongoose = require('mongoose');
var Agenda = mongoose.model('Agenda');
var User 	= mongoose.model('User');

/**
 * Create
 */
exports.create = function (req, res, next) {
	var agenda = new Agenda(req.body);
	agenda.setUser(req.user)
	agenda.save(function (err) {
		if (err) return next(err);
		res.json(agenda);
	});
};

/**
 * Index
 */
exports.index = function (req, res, next) {
  var page = req.query.page ? req.query.page : 1;
  var per_page = 10;
  var total = 0;
	var query = { account: req.user.account };
	var search = req.query.search;

	if (search) {
   var regex = new RegExp(search, "i");
   query.fullName = regex;
  }

	Agenda.find(query)
	.sort('-date')
	.skip((page-1)*per_page).limit(per_page)
	.populate('patient', '_id fullName')
	.exec(function (err, agenda) {
		if (err) return next(err);
		Agenda.count(query, function (err, count) {
			if (err) return next(err);
			res.json({ data: agenda, count: count });
		});
	});
}

/**
 * Find
 */
exports.get = function (req, res, next) {
	Agenda
	.findById(req.params.id)
	.populate('patient')
	.exec(function (err, appointment) {
		if (err) return next(err);
		res.json(appointment);
	})
};

/**
 * Update
 */
exports.update = function (req, res, next) {
	Agenda.findById(req.params.id, function (err, agenda) {
		if (err) return next(err);
		agenda.update(req.body, function (err) {
			if (err) return next(err);
			res.json({ message: 'Updated' });
		});
	});
};

/**
 * Delete
 */
exports.delete = function (req, res, next) {
	Agenda.findByIdAndRemove(req.params.id, function(err) {
    if (err) { return next(err)};
    res.json({ message: 'Deleted' });
  });
};
