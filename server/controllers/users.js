var mongoose 	= require('mongoose');
var User 	= mongoose.model('User');
var $q = require('q');

/**
 * Create
 */
exports.create = function (req, res, next) {

  if(!req.body.firstName || !req.body.username || !req.body.lastName || !req.body.role || !req.body.password) {
		return res.status(400).json({ message: 'Please fill out all fields required.', type : "danger"});
	}

  if(req.body.password !== req.body.confirmPassword) {
		return res.status(400).json({ message: "Passwords do not match. Please enter your password again." , type : "danger" });
	}

  var result = $q.defer()
  var user = req.user;

  User.find({ username: req.body.username }).exec(function(err, users) {
		if(users.length > 0){
        result.reject({ message:"This username already exist. Please enter another username." , type : "danger" });
		} else {
      result.resolve()
    }
  });

  result.promise.then(function () {

  	var user = new User(req.body);
  	user.setUser(req.user);
    user.setPassword(req.body.password);
    user.save(function (err) {
  		if (err) return next(err);
  		res.json(user);
  	});

  }).fail(function (err) {
    return res.status(400).json(err);
  })
};

/**
 * Index
 */
exports.index = function (req, res, next) {
  var page = req.query.page ? req.query.page : 1;
  var per_page = 10;
  var total = 0;
  var search = req.query.search;
	var query = {
    account: req.user.account,
    _id: { $ne: req.user.id },
    role: { $ne: 'master' }
  };

	if (search) {
   var regex = new RegExp(search, "i");
   query.fullName = regex;
  }

	User.find(query, function (err, users) {
		if (err) return next(err);
		User.count(query, function (err, count) {
			if (err) return next(err);
			res.json({ data: users, count: count });
		});
	}).sort('-firstName').skip((page-1)*per_page).limit(per_page);
}

/**
 * Find
 */
exports.get = function (req, res, next) {
	User.findById(req.params.id, function (err, user) {
		if (err) return next(err);
		res.json(user);
	});
};

/**
 * Update
 */
exports.update = function (req, res, next) {

  if(!req.body.firstName || !req.body.username || !req.body.lastName || !req.body.role) {
    return res.status(400).json({ message: 'Please fill out all fields required.', type : "danger"});
  }

  if(req.body.password !== req.body.confirmPassword) {
    return res.status(400).json({ message: "Passwords do not match. Please enter your password again." , type : "danger" });
  }

  var result = $q.defer()
  var username = req.body.username;
  var user = req.params.id;

  User.find({ _id: { $ne: user }, username: username }).exec(function(err, users) {
    if(users.length){
        result.reject({ message:"This username already exist. Please enter another username." , type : "danger" });
    } else {
      result.resolve()
    }
  });

  result.promise.then(function () {
    User.findById(req.params.id, function (err, user) {
  		if (err) return next(err);
      if (req.body.password) {
        let salt = user.getSalt();
        req.body.salt = salt;
        req.body.hash = user.getHash(req.body.password, salt);
      }
  		req.body.fullName = user.getFullName(req.body.firstName, req.body.lastName);
  		user.update(req.body, function (err) {
  			if (err) return next(err);
  			res.json({ message: 'Updated' });
  		});
  	});
  }).fail(function (err) {
    return res.status(400).json(err);
  })
};

/**
 * Delete
 */
exports.delete = function (req, res, next) {
	User.findByIdAndRemove(req.params.id, function(err) {
    if (err) { return next(err)};
    res.json({ message: 'Deleted' });
  });
};
