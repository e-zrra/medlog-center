var mongoose 	= require('mongoose');
var Patient = mongoose.model('Patient');
var User 	= mongoose.model('User');
var Agenda = mongoose.model('Agenda');
var uploadFile = require('../util/uploadFile');

/**
 * Create
 */
exports.create = function (req, res, next) {
	var patient = new Patient(req.body);
	patient.setUser(req.user);
	patient.save(function (err) {
		if (err) return next(err);
		res.json(patient);
	});
};

/**
 * Index
 */
exports.index = function (req, res, next) {
  var page = req.query.page ? req.query.page : 1;
  var per_page = 10;
	var query = { account: req.user.account };
	var search = req.query.search;

	if (search) {
   var regex = new RegExp(search, "i");
   query.fullName = regex;
  }

	Patient.find(query, function (err, patients) {
		if (err) return next(err);
		Patient.count(query, function (err, count) {
			if (err) return next(err);
			res.json({ data: patients, count: count });
		});
	}).sort('-firstName').skip((page-1)*per_page).limit(per_page);
}

/**
 * All
 */
exports.all = function (req, res, next) {
	var account = req.user.account;
	var query = { account: account };

	Patient.find(query, '_id fullName', function (err, patients) {
		if (err) return next(err);
		res.json(patients);
	}).sort('-firstName').limit(10);
}

/**
 * Search
 */
exports.search = function (req, res, next) {
	var account = req.user.account;
	var query = { account: account };
	var search = req.query.search;
	if (search) {
   var regex = new RegExp(search, "i");
   query.fullName = regex;
  }
	Patient.find(query, function (err, patients) {
		if (err) return next(err);
		res.json(patients);
	}).sort('-firstName').limit(10);
}

/**
 * Get
 */
exports.get = function (req, res, next) {
	Patient.findById(req.params.id, function (err, patient) {
		if (err) return next(err);
		res.json(patient);
	});
};

/**
 * Update
 */
exports.update = function (req, res, next) {
	Patient.findById(req.params.id, function (err, patient) {
		if (err) return next(err);
		req.body.fullName = patient.getFullName(req.body.firstName, req.body.lastName)
		patient.update(req.body, function (err) {
			if (err) return next(err);
			res.json({ message: 'Updated' });
		});
	});
};

/**
 * Delete
 */
exports.delete = function (req, res, next) {
	Patient.findByIdAndRemove(req.params.id, function(err) {
    if (err) { return next(err)};
    res.json({ message: 'Deleted' });
  });
};

/**
 * Agenda
 */
exports.agenda = function (req, res, next) {
	var query = { patient: req.params.id };

	Agenda.find(query, function (err, appointments) {
		if (err) return next(err);
		res.json(appointments);
	})
}

/**
 * Update medical history
 */
exports.medicalHistoryUpdate = function (req, res, next) {
	let patientUpdate = req.body;
	Patient.findOneAndUpdate({ _id: req.params.id }, patientUpdate, function (err, patient) {
		if (err) return next(err);
		res.json({ message: 'Updated' });
	});
};

/**
 * Create file
 */
exports.fileCreate = function (req, res, next) {

	var maxFiles = 10;
  var patientId = req.params.id;
  var user = req.user;
  var destination = `accounts/${user.accountId}/files/`;
  var upload = uploadFile.upload(destination);

	Patient.findById(patientId, function (err, patient) {
		if (err) res.status(500).send();
		if (patient.files.length > maxFiles) res.json({ success: false, message: "No puedes agregar mas archivos, excediste de 10 archivos. Eliminar archivos." });

		upload(req, res, function (err) {
			if (err || !req.file) res.status(500).send();
	    var file = req.file;
	    var file = {
				'_id': mongoose.Types.ObjectId(),
				'name': file.originalname,
	      'path': file.path,
	      'filename': file.filename,
	      'mimetype': file.mimetype,
	      'destination': destination,
				'createdAt': Date.now()
	    };

	    Patient.findByIdAndUpdate(patientId, { $push: { "files": file } }, function (err) {
	      if (err) res.status(500).send();
	      res.json({ success: true, file: file })
	    })
		})
	})
}

exports.fileDestroy = function (req, res, next) {

	var patientId = req.params.patientId;
	var id = req.params.id;

	Patient.findById(patientId, function (err, patient) {
		if (err) res.status(500).send();

		var file = { _id: id };
		Patient.findByIdAndUpdate(patientId, { $pull: { "files": file } }, function (err) {
			if (err) res.status(500).send();
			res.json({ success: true, file: file })
		})

	})

}
