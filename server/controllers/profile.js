var mongoose 	= require('mongoose');
var Account = mongoose.model('Account');
var User 	= mongoose.model('User');
var $q = require('q');

exports.get = function (req, res, next) {
  var user = req.user.id;

	User.findById(user, function (err, user) {
		if (err) return next(err);
		res.json(user);
	});
};

exports.update = function (req, res, next) {

  if(!req.body.firstName || !req.body.username || !req.body.lastName) {
		return res.status(400).json({ message: 'Please fill out all fields required.'});
	}

  if(req.body.password !== req.body.confirmPassword) {
		return res.status(400).json({ message: "Passwords do not match. Please enter your password again." , type : "danger" });
	}

  var result = $q.defer()
  var user = req.user.id;

  User.find({ _id: { $ne: user }, username: req.body.username }).exec(function(err, users) {
		if(users.length > 0){
        result.reject({ message:"This username already exist. Please enter another username." , type : "danger" })
      // return res.status(400).json({ message: 'This username already exist. Please enter another username.', type : "danger"});
		} else {
      result.resolve()
    }
  });

  result.promise.then(function () {

  	User.findById(user, function (err, user) {
  		if (err) return next(err);

      if (req.body.password) {
        let salt = user.getSalt();
        req.body.salt = salt;
        req.body.hash = user.getHash(req.body.password, salt);
      }
  		req.body.fullName = user.getFullName(req.body.firstName, req.body.lastName);

  		user.update(req.body, function (err) {
  			if (err) return next(err);
  			res.json({ message: 'Updated' });
  		});
  	});

  }).fail(function (err) {
    return res.status(400).json(err);
  })
};
