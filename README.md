# Medlog Center Application

### You need:

* Node.js
* Node Packaged Modules (npm)
* Bower
* MongoDB

Install node modules and dependencies

Node Modules
```
$ npm install
```

Dependencies with Bower
```
$ bower install
```

Make sure that MongoDB Server is running, using _mongod process_
```
$ mongod
```

And run the server
```
$ npm start
```

or run the app like development
```
$ npm run dev
```
