(function () {
'use strict';
	angular
		.module('app')
		.controller('AppCtrl', AppCtrl);

	AppCtrl.$inject = ['$scope', 'Auth', 'Logout'];

	function AppCtrl ($scope, Auth, Logout) {
    $scope.isLoggedIn = Auth.isLoggedIn;
		$scope.logOut = Logout.logOut;
		$scope.currentUser = Auth.currentUser();
  }

})();
