(function () {
'use strict';
	angular
		.module('app')
		.controller('PatientCtrl', PatientCtrl);

	PatientCtrl.$inject = ['$scope', 'Patient', '$log', 'toastr'];

	function PatientCtrl ($scope, Patient, $log, toastr) {

		$scope.patients = [];

		$scope.pagination = {
			currentPage: 1
		}

		paginateMethod()

		$scope.search = function () {
			$scope.pagination = {
				currentPage: 1
			}
	    paginateMethod()
	  }

		$scope.pageChanged = function(text, page, pageSize, total) {
			$scope.pagination.currentPage = page;
			paginateMethod()
    };

		function paginateMethod () {
			let search = ($scope.input) ? $scope.input : null;
    	let currentPage = $scope.pagination.currentPage;

			Patient.index(currentPage, search)
			.then(function (response) {
				$scope.patients = response.data;
				$scope.pagination = {
					currentPage: currentPage,
					pageSize: 10,
					total: response.count
				}
			}).catch(function (response) {
				$scope.patients = 0;
				toastr.error('Registros no encontrados.');
			});
		}

	}

})();
