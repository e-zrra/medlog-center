(function () {
'use strict';
  angular
    .module('app')
    .controller('PatientEditCtrl', PatientEditCtrl);

  PatientEditCtrl.$inject = ['$scope', 'Patient', '$stateParams', 'Country', 'toastr', '$state'];

  function PatientEditCtrl ($scope, Patient, $stateParams, Country, toastr, $state) {

    let id = $stateParams.id;
    $scope.patient = {};

    Patient.get(id)
      .then(function (patient) {
          $scope.patient = patient;
      }).catch(function () {
        toastr.error('Registro no encontrado.');
      });

    Country.all()
      .then(function (countries) {
        $scope.countries = countries;
      }).catch(function () {
        toastr.error('Registros no encontrados.');
      })

    $scope.update = function (patient) {
      return Patient.update(id, patient)
        .success(function (response) {
          $state.go('patients-medical-history', {id: id})
          toastr.success('Registro actualizado correctamente.');
        }).error(function () {
          toastr.error('Registro no se pudo actualizar.');
        });
    }
  }

})();
