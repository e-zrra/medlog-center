(function () {
'use strict';
	angular
		.module('app')
		.controller('PatientMedicalHistoryCtrl', PatientMedicalHistoryCtrl);

	PatientMedicalHistoryCtrl.$inject = ['$scope', 'Patient', 'Consultation','$location', '$stateParams', 'toastr', 'Upload', '$timeout'];

	function PatientMedicalHistoryCtrl ($scope, Patient, Consultation, $location, $stateParams, toastr, Upload, $timeout) {

		let patientId = $stateParams.id;

		$scope.consultations = [];
		$scope.appointments = [];
		$scope.patient = {};
		$scope.maxSize = 5;
		$scope.maxFiles = 10;

		$scope.pagination = {
			currentPage: 1
		}

		$scope.fileRemove = function (id) {
			$scope.patient.files.splice(id, 1);
		}

		/* Script custom for tabs */
		$scope.view_tab = 'tab1';
		$scope.changeTab = function(tab) {
			$scope.view_tab = tab;
		}

		$scope.pageChanged = function(text, page, pageSize, total) {
			$scope.pagination.currentPage = page;
			paginateMethod()
    };

		paginateMethod()

		Patient.get(patientId)
			.then(function (patient) {
				$scope.patient = patient;
			}).catch(function () {
				$scope.patient = 0;
				toastr.error('Registro no encontrado.', 'Paciente');
			});

	  function paginateMethod () {
	    var currentPage = $scope.pagination.currentPage;
			Consultation.consultationsByPatient(patientId, currentPage)
				.then(function (response) {
					$scope.consultations = response.data;
					$scope.pagination = {
						currentPage: currentPage,
						pageSize: 10,
						total: response.count
					}
				}).catch (function () {
					$scope.consultations = 0;
					toastr.error('Registros no encontrados.');
				})
	  }

		$scope.updateMedicalHistory = function () {
			return Patient.updateMedicalHistory(patientId, $scope.patient)
				.success(function (response) {
					toastr.success('Registro actualizado correctamente.');
        }).error(function () {
					toastr.error('Registro no se pudo actualizar.');
        });
		};

		/**
     * @name Get appointments of the patient
     * @desc
     * @return Void
     */
    Patient.agenda(patientId)
      .then(function (appointments) {
        $scope.appointments = appointments;
      }).catch(function (response) {
        toastr.error('Registros no encontrados (Agenda).', 'Agenda');
      });

		$scope.uploadFile = function(files, errFiles) {
			if (errFiles.length) {
				$scope.errFiles = errFiles;
				return false;
			}
			var url = `/api/patients/${patientId}/files/create/`;
      $scope.files = files;
      $scope.errFiles = errFiles;

      angular.forEach(files, function(file) {
        file.upload = Upload.upload({ url: url, data: {file: file} });
        file.upload.then(function (response) {
          $timeout(function () {
						if (response.data.success) {
							$scope.patient.files.push(response.data.file)
							toastr.success("Archivo adjuntado");
						}
						else if (!response.data.success && response.data.message) {
							toastr.error(response.data.message);
						} else {
							toastr.error("Archivo no se pudo adjuntar");
						}
          });
        }, function (response) {
          if (response.status > 0)
            $scope.errorMsg = response.status + ': ' + response.data;
        }, function (evt) {
          file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
      });
    }

	}

})();
