(function () {
'use strict';
	angular
		.module('app')
		.controller('PatientCreateCtrl', PatientCreateCtrl);

	PatientCreateCtrl.$inject = ['$scope', 'Patient', '$state', 'toastr'];

	function PatientCreateCtrl ($scope, Patient, $state, toastr) {

		$scope.patient = {};

		$scope.create = function () {
			let patient = $scope.patient;
			return Patient.create(patient)
				.success(function (data) {
					$state.go('patients-medical-history', {id: data._id});
					toastr.success('Registro creado correctamente.');
				}).error(function (data) {
					toastr.error('Registro no se pudo crear.');
				});
		};
	}

})();
