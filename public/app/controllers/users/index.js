(function () {
'use strict';
	angular
		.module('app')
		.controller('UsersCtrl', UsersCtrl);

	UsersCtrl.$inject = ['$scope', 'User', 'toastr'];

	function UsersCtrl ($scope, User, toastr) {

		$scope.users = [];

		$scope.pagination = {
			currentPage: 1
		}

		paginateMethod()

		$scope.search = function () {
			$scope.pagination = {
				currentPage: 1
			}
	    paginateMethod()
	  }

		$scope.pageChanged = function(text, page, pageSize, total) {
			$scope.pagination.currentPage = page;
			paginateMethod()
    };

		function paginateMethod () {
			let search = ($scope.input) ? $scope.input : null;
    	let currentPage = $scope.pagination.currentPage;

			User.index(currentPage, search)
			.then(function (response) {
				$scope.users = response.data;
				$scope.pagination = {
					currentPage: currentPage,
					pageSize: 10,
					total: response.count
				}
			}).catch(function (error) {
				$scope.users = 0;
				toastr.error('Registros no encontrados.', 'Usuarios');
			});
		}

	}
})();
