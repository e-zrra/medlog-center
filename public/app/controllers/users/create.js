(function () {
'use strict';
	angular
		.module('app')
		.controller('UserCreateCtrl', UserCreateCtrl);

	UserCreateCtrl.$inject = ['$scope', 'User', '$state', 'toastr'];

	function UserCreateCtrl ($scope, User, $state, toastr) {

		$scope.user = {};

		$scope.create = function () {
			let user = $scope.user;
			return User.create(user)
				.success(function (response) {
					$state.go('users');
					toastr.success('Registro creado correctamente.');
				}).error(function (response) {
					toastr.error('Registro no se pudo crear.');
					if (response.message) {
						toastr.warning(response.message);
					}
				});
		};
	}

})();
