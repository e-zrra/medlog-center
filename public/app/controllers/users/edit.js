(function () {
'use strict';
  angular
    .module('app')
    .controller('UserEditCtrl', UserEditCtrl);

  UserEditCtrl.$inject = ['$scope', 'User', '$stateParams', 'toastr', '$state'];

  function UserEditCtrl ($scope, User, $stateParams, toastr, $state) {

    let id = $stateParams.id;
    $scope.user = {};

    User.get(id)
      .then(function (data) {
          $scope.user = data;
      }).catch(function () {
        toastr.error('Registro no encontrado.');
      });

    $scope.update = function (patient) {
      return User.update(id, patient)
        .success(function (response) {
          toastr.success('Registro actualizado correctamente.');
        }).error(function (response) {
          toastr.error('Registro no se pudo actualizar.');
					if (response.message) {
						toastr.warning(response.message);
					}
        });
    }
  }

})();
