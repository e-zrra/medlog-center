(function () {
'use strict';
	angular
		.module('app')
		.controller('WebsiteCtrl', WebsiteCtrl);

	WebsiteCtrl.$inject = ['$scope', 'Auth', '$window', 'Logout'];

	function WebsiteCtrl ($scope, Auth, $window, Logout) {
		
    $scope.isLoggedIn = Auth.isLoggedIn;
		$scope.logOut = Logout.logOut;

  }
})();
