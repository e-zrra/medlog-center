(function () {
'use strict';
	angular
		.module('app')
		.controller('AgendaEditCtrl', AgendaEditCtrl);

	AgendaEditCtrl.$inject = ['$scope', 'Agenda', 'Patient', '$state', 'toastr', '$stateParams'];

	function AgendaEditCtrl ($scope, Agenda, Patient, $state, toastr, $stateParams) {

    let id = $stateParams.id;
    $scope.appointment = {};
    $scope.hstep = 1;
	  $scope.mstep = 1;
		$scope.patients = {};
		$scope.appointment = {};
		$scope.ismeridian = true;
		$scope.minDate = moment(new Date()).subtract(1, 'days');

    Agenda.get(id)
      .then(function (data) {
          $scope.appointment = data;
          $scope.selected = data.patient.fullName;
					$scope.appointment.patient = data.patient._id;
      }).catch(function () {
        toastr.error('Registro no encontrado (Cita).');
      });

		$scope.onSelect = function ($item, $model, $label) {
			$scope.appointment.patient = $item._id;
		}

		Patient.all()
			.then(function (response) {
				$scope.patients = response;
			}).catch(function (response) {
				toastr.error('Registros no encontrados (Pacientes).');
			});

		$scope.update = function () {
			let appointment = $scope.appointment;
			return Agenda.update(id, appointment)
				.success(function (data) {
					$state.go('agenda');
					toastr.success('Registro actualizado correctamente.');
				}).error(function (data) {
					toastr.error('Registro no se pudo actualizar.');
				});
		};
	}
})();
