(function () {
'use strict';
	angular
		.module('app')
		.controller('AgendaCreateCtrl', AgendaCreateCtrl);

	AgendaCreateCtrl.$inject = ['$scope', 'Agenda', 'Patient', '$state', 'toastr', '$http'];

	function AgendaCreateCtrl ($scope, Agenda, Patient, $state, toastr, $http) {

		$scope.hstep = 1;
	  $scope.mstep = 1;
		$scope.patients = {};
		$scope.appointment = {};
		$scope.ismeridian = true;
		$scope.minDate = moment(new Date()).subtract(1, 'days');

		$scope.onSelect = function ($item, $model, $label) {
			$scope.appointment.patient = $item._id;
		}

		Patient.all()
			.then(function (response) {
				$scope.patients = response;
			}).catch(function (response) {
				toastr.error('Registros no encontrados (Pacientes).');
			});

		$scope.create = function () {
			let appointment = $scope.appointment;
			return Agenda.create(appointment)
				.success(function (data) {
					$state.go('agenda');
					toastr.success('Registro creado correctamente.');
				}).error(function (data) {
					toastr.error('Registro no se pudo crear.');
				});
		};
	}

})();
