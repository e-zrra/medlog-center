(function () {
'use strict';
	angular
		.module('app')
		.controller('AgendaCtrl', AgendaCtrl);

	AgendaCtrl.$inject = ['$scope', 'Agenda', 'toastr'];

	function AgendaCtrl ($scope, Agenda, toastr) {

		$scope.appointments = [];

		$scope.pagination = {
			currentPage: 1
		}

		paginateMethod()

		$scope.search = function () {
			$scope.pagination = {
				currentPage: 1
			}
	    paginateMethod()
	  }

		$scope.pageChanged = function(text, page, pageSize, total) {
			$scope.pagination.currentPage = page;
			paginateMethod()
    };

		function paginateMethod () {
			let search = ($scope.input) ? $scope.input : null;
    	let currentPage = $scope.pagination.currentPage;

			Agenda.index(currentPage, search)
			.then(function (response) {
				$scope.appointments = response.data;
				$scope.pagination = {
					currentPage: currentPage,
					pageSize: 10,
					total: response.count
				}
			}).catch(function (error) {
				$scope.appointments = 0;
				toastr.error('Registros no encontrados.', 'Usuarios');
			});
		}

	}
})();
