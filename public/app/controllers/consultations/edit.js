/**
 * Consultation Controller
 * @namespace Controller
 */
(function() {
'use strict';
  angular
    .module('app')
    .controller('ConsultationEditCtrl', ConsultationEditCtrl);

  ConsultationEditCtrl.$inject = ['$scope', 'Consultation', 'Patient', '$stateParams', 'toastr', '$compile', '$templateRequest'];

  /**
   * @name Edit consultation
   * @desc Edit the consultation
   * @param {Object} scope
   * @param {Factory} Consultation
   * @param {Factory} Patient
   * @param {Object} State Params
   * @param {Directive} Toastr
   * @param {Object} Compile
   * @param {Object} Template Request
   */
  function ConsultationEditCtrl ($scope, Consultation, Patient, $stateParams, toastr, $compile, $templateRequest) {

    let id = $stateParams.id;
    $scope.patient = {};
    $scope.consultation = {};
    $scope.medicine = {};
    $scope.consultation.medicine = [];

    Consultation.get(id)
      .then(function (consultation) {
        $scope.consultation = consultation;
        if (!consultation.patient) {
          toastr.error('Registro no encontrado.', 'Paciente');
        }
        $scope.patient = consultation.patient;
      }).catch(function (response) {
        toastr.error('Registro no encontrado.', 'Consulta médica');
      });

    /**
     * @name Update consultation
     * @desc Update the consultation
     * @param {Object} Consultation
     * @return Void
     */
    $scope.update = function (consultation) {
      Consultation.update(id, consultation)
        .success(function (data) {
          toastr.success('Registro actualizado correctamente.')
        }).error(function () {
          toastr.error('Registro no se pudo actualizar.')
        });
    }


  }
})();
