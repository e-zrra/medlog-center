(function () {
'use strict';
	angular
		.module('app')
		.controller('ProfileCtrl', ProfileCtrl);

	ProfileCtrl.$inject = ['$scope', 'Profile', 'toastr'];

	function ProfileCtrl ($scope, Profile, toastr) {

    $scope.user = {};

    Profile.get().then(function (response) {
      $scope.user = response;
    }).catch(function (error) {
      $scope.patients = 0;
      toastr.error('Registro no encontrado.');
    });

		$scope.update = function () {
			return Profile.update($scope.user)
        .success(function (response) {
					toastr.success('Registro actualizado correctamente.');
        }).error(function (response) {
					toastr.error('Registro no se pudo crear.');
					if (response.message) {
						toastr.warning(response.message);
					}
        });
		}

	}

})();
