(function () {
'use strict';
	angular
		.module('app')
		.controller('AuthCtrl', AuthCtrl);

	AuthCtrl.$inject = ['$scope', 'Auth', 'Login', 'Register', '$window'];

	function AuthCtrl ($scope, Auth, Login, Register, $window) {

		// if (Auth.isLoggedIn()) $window.location.href = 'panel';

		var button = angular.element(document.querySelector('#button'));

    $scope.credentials = {};
		$scope.message = "";
		$scope.user = {}

    $scope.signIn = function () {
			button.attr("disabled", true);
      Register
        .register($scope.credentials)
        .error(function(err) {
					$scope.message = err.message;
					button.attr("disabled", false);
        })
        .then(function(res) {
					button.attr("disabled", false);
					$window.location.href = 'panel';
        }).catch(function (res) {
					$scope.message = res.data.message;
					button.attr("disabled", false);
				});
    };

    $scope.logIn = function () {
			button.attr("disabled", true);
			Login.logIn($scope.user).error(function(error){
				$scope.message = error.message;
				button.attr("disabled", false);
			}).then(function(res) {
				button.attr("disabled", false);
				$window.location.href = 'panel';
			}).catch(function (res) {
				$scope.message = res.data.message;
				button.attr("disabled", false);
			});
		};
  }

})();
