(function () {
'use strict';
	angular
		.module('app')
		.factory('Auth', auth);

	auth.$inject = ['$window'];

	function auth ($window) {
    let auth = {};

  	auth.saveToken = function (token) {
    		$window.localStorage['token'] = token;
  	};

  	auth.getToken = function () {
    		return $window.localStorage['token'];
  	}

  	auth.isLoggedIn = function () {
			var token = auth.getToken();
      var payload;

      if(token){
        payload = token.split('.')[1];
        payload = $window.atob(payload);
        payload = JSON.parse(payload);

        return payload.exp > Date.now() / 1000;
      } else {
        return false;
      }
  	};

  	auth.currentUser = function () {
			if(auth.isLoggedIn()){
        var token = auth.getToken();
        var payload = token.split('.')[1];
        payload = $window.atob(payload);
        payload = JSON.parse(payload);
				return payload;
      }
  	};

		// auth.ensureAuthenticated = function (token) {
		// 	return $http({
		//     method: 'GET',
		//     url: baseURL + 'user',
		//     headers: {
		//       'Content-Type': 'application/json',
		//       Authorization: 'Bearer ' + token
		//     }
		//   });
		// }

		// usage

		// if (token) {
	 	//  Auth.ensureAuthenticated(token)
	 	// 	.then(function(user) {
	 	//   	// if (user.data.status === 'success');
	 	//   	// vm.isLoggedIn = true;
	 	// 		console.log(user)
	 	// 	})
	 	// 	.catch(function(err) {
	 	//   	console.log(err);
	 	// 	});
	 	// }

  	return auth;
  }

})();
