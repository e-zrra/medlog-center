(function () {
'use strict';
	angular
		.module('app')
		.factory('TokenInterceptor', ['$q', '$window', '$location', 'Auth', function ($q, $window, $location, Auth) {

	    return {
	       request: function (config) {
	           config.headers = config.headers || {};
						 var token = $window.localStorage.token;
	           if (token) {
	               config.headers.Authorization = 'Bearer ' + $window.localStorage.token;
	           }

	           return config;
	       },
	       requestError: function(rejection) {
	           return $q.reject(rejection);
	       },
	       /* Set Authentication.isAuthenticated to true if 200 received */
	       response: function (response) {
					//  console.log(response.status)
					//  	if (response.status === 401) { // Unauthorized
					// 			$window.location.href = 'login';
					// 	}
					// 	if (response.status === 403) { // Not token
					// 			$window.location.href = 'login';
					// 	}

	          if (response != null && response.status == 200 && $window.localStorage.token && !Auth.isLoggedIn) {
	             // Auth.isAuthenticated = true;
	          }

						return response || $q.when(response);
	       },
	       /* Revoke client authentication if 401 is received */
	       responseError: function(rejection) {
           if (rejection != null && rejection.status === 401 && ($window.localStorage.token || Auth.isLoggedIn)) {
             delete $window.localStorage.token;
             // AuthenticationService.isAuthenticated = false;
             // $location.path("/login");
           }
           return $q.reject(rejection);
	       }
	   };

		}]);

})();
