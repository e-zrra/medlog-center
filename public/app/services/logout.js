(function () {
'use strict';
	angular
		.module('app')
		.factory('Logout', logout);

  logout.$inject = ['$http', '$window'];

	function logout ($http, $window, Auth) {

    var o = {};

    o.logOut = function () {
  	  $window.localStorage.removeItem('token');
			$window.location.href = 'login';
  	};

    return o;

  }

})();
