(function () {
'use strict';
	angular
		.module('app')
		.factory('Login', login);

  login.$inject = ['$http', '$window', 'Auth'];

	function login ($http, $window, Auth) {

    var o = {};

    o.logIn = function (user) {
  	  return $http.post('/login', user).success(function(data){
  	    Auth.saveToken(data.token);
  	  });
  	};

    return o;

  }

})();
