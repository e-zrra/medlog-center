(function () {
'use strict';
	angular
		.module('app')
		.factory('Register', register);

  register.$inject = ['$http', '$window', 'Auth'];

	function register ($http, $window, Auth) {

    var o = {};

    o.register = function (user) {
  	  return $http.post('/register', user).success(function(res) {
				Auth.saveToken(res.token);
			});
  	};

    return o;

  }

})();
