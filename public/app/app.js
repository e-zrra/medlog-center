angular
	.module('app', [
		'ui.bootstrap'
		, 'ui.router'
		, 'ui.mask'
		, 'ngNotify'
		, 'bw.paging'
		, 'toastr'
		, 'ngFileUpload'
]);
