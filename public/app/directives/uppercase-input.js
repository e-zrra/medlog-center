(function () {
'use strict';
	angular
		.module('app')
		.directive('uppercase', uppercase);

	function uppercase () {
		return {
      restrict: "A",
			require: '?ngModel',
			link: function (scope, elem, attrs, ngModel) {
        ngModel.$parsers.push(function(input) {
            return input ? input.toUpperCase() : "";
        });
        elem.css("text-transform", "uppercase");
			}
		}
	}

})()
