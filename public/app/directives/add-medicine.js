(function () {
'use strict';
	angular
		.module('app')
		.directive('addMedicine', addMedicine);

	function addMedicine ($compile, $templateRequest, toastr) {
    return {
      restrict: 'AE',
      link: function(scope, element, attrs) {
				var count = scope.consultation.medicines.length | 0;
				var content = angular.element(document.getElementById("medicines-content"));

				scope.closeAlert = function(index) {
			    scope.consultation.medicines.splice(index, 1);
					toastr.info('Medicamento removido. Guarda los cambios para eliminar definitivamente.');
			  };

				element.on('click', function(event) {

					var li = '<li class="list-group-item"> \
											<div class="row"> \
												<div class="col-md-4"> \
													<div class="form-group"> \
														<label>Medicamento</label> \
														<input ng-model="consultation.medicines['+ count +'].name" type="text" class="form-control" placeholder="Medicamento"/> \
													</div> \
												<div class="row"> \
													<div class="col-md-4"> \
														<label>Dosis</label> \
														<input ng-model="consultation.medicines['+ count +'].dose" type="text" class="form-control" placeholder="2"/> \
													</div> \
													<div class="col-md-8"> \
														<label>Duración</label> \
														<input ng-model="consultation.medicines['+ count +'].duration" type="text" class="form-control" placeholder=""/> \
													</div> \
												</div> \
											</div> \
											<div class="col-md-8"> \
												<div class="form-group"> \
													<label>Comentarios</label> \
													<textarea ng-model="consultation.medicines['+ count +'].comments" class="form-control" rows="5"></textarea> \
												</div> \
												<a class="btn btn-danger btn-sm pull-right"> \
												<i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar </a> \
											</div> \
										</div> \
									</li>';

					var html = angular.element(li);
					var compile = $compile(html)(scope);
					scope.consultation.medicines[count] = angular.copy(scope.medicine);
					content = angular.element(document.getElementById("medicines-content"));
					scope.medicine = {};
					count++;
					toastr.info('Medicamento agregado al final de la lista. Guarda los cambios para almacenarlo.');
		    })

      }
    }
	}

})()
