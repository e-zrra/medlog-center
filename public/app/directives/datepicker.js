(function () {
'use strict';
	angular
		.module('app')
		.directive('datepicker', datepicker);

	function datepicker () {

		return {
			restrict: 'E',
			scope: {
				ngModel: '=',
				format: '@',
				minDate: '@'
			},
			templateUrl: 'app/views/directives/datepicker.html',
			link: function (scope, element, attrs) {

				scope.clearText = "Limpiar";
				scope.closeText = 'Cerrar';
				scope.currentText = "Hoy";

				scope.ngModel = scope.ngModel;

				scope.today = function() {
					scope.ngModel = new Date();
        };

    		scope.clear = function() {
        	scope.ngModel = null;
    		};

        scope.dateOptions = {
          // maxDate: new Date(2020, 5, 22),
          minDate: new Date(moment(scope.minDate, 'YYYY-MM-DD').format()),
          startingDay: 1
        };

        // scope.toggleMin = function() {
        //   scope.inlineOptions.minDate = scope.inlineOptions.minDate ? null : new Date();
        //   scope.dateOptions.minDate = scope.inlineOptions.minDate;
        // };
				//
        // scope.toggleMin();

        scope.open2 = function() {
          scope.popup2.opened = true;
        };

        scope.popup2 = {
          opened: false
        };
			}
		}
	}

})();
