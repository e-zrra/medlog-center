(function () {
'use strict';
	angular
		.module('app')
		.directive('clickAndDisable', clickAndDisable);

	function clickAndDisable () {

		return {
			scope: {
				clickAndDisable: '&'
			},
			link: function (scope, elem, attrs) {

				elem.bind('submit', function () {

					var buttons = angular.element(elem[0].getElementsByClassName('btn'));

					buttons.prop('disabled', true);
					buttons.attr('disabled', true);

					scope
						.clickAndDisable()
						.finally(function () {

							buttons.prop('disabled', false);
							buttons.attr('disabled', false);
						})
				});
			}
		}
	}

})()
