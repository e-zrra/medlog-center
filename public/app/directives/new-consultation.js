(function () {
'use strict';
	angular
		.module('app')
		.directive('newConsultation', newConsultation);

	newConsultation.$inject = ['$uibModal'];

	function newConsultation ($uibModal) {

		return {
	    restrict: 'EA',
			scope: {
				patientId: '=',
				classValue: '@'
			},
			templateUrl: 'app/views/directives/new-consultation.html',
			link: function (scope, element, attrs) {
				scope.openModalConsultation = function () {
				  var modalInstance = $uibModal.open({
			      	animation: true,
			      	ariaLabelledBy: 'modal-title',
			      	ariaDescribedBy: 'modal-body',
			      	templateUrl: 'modelContentConsultation.html',
			      	controller: 'ModalInstanceConsultationCtrl',
			      	resolve: {
			      		patientId: function () {
			      			return scope.patientId;
			    			}
			    		}
			    });
				};
	    }
	  };
	}

	angular
		.module('app')
		.controller('ModalInstanceConsultationCtrl', ModalInstanceConsultationCtrl);

		ModalInstanceConsultationCtrl.$inject = ['$scope', '$uibModalInstance', 'patientId', 'Consultation', '$state', 'toastr'];

		function ModalInstanceConsultationCtrl ($scope, $uibModalInstance, patientId, Consultation, $state, toastr) {

			$scope.createConsultation = function () {
				var title = $scope.titleConsultation;

				return Consultation.create(title, patientId)
					.success(function (response) {
						$state.go('consultations-edit', {id: response.data._id});
						$uibModalInstance.dismiss('cancel');
						toastr.success('Registro creado correctamente.')
					}).error(function (response) {
						toastr.error('Registro no se pudo crear.');
					});
			}

	  	$scope.cancelModalConsultation = function () {
	    	$uibModalInstance.dismiss('cancel');
	  	};
		}

})();
