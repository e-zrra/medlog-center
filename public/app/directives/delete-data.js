(function () {
'use strict';
	angular
		.module('app')
		.directive('deleteData', deleteData);

	deleteData.$inject = ['$uibModal'];

	function deleteData ($uibModal) {

		return {
	    restrict: 'EA',
			scope: {
				recordId: '=',
				recordPaterId: '=',
				text: '@',
				classValue: '@',
        redirect: '@',
				redirectParams: '=',
        serviceName: '@',
				callback: '&'
			},
			templateUrl: 'app/views/directives/delete-data.html',
			link: function (scope, element, attrs) {

				scope.openModalDeleteData = function () {
				  var modalInstance = $uibModal.open({
			      	animation: true,
			      	ariaLabelledBy: 'modal-title',
			      	ariaDescribedBy: 'modal-body',
			      	templateUrl: 'modelContentDeleteData.html',
			      	controller: 'ModalInstanceDeleteDataCtrl',
			      	resolve: {
			      		recordId: function () {
			      			return scope.recordId;
			    			},
								recordPaterId: function () {
									return scope.recordPaterId
								},
                redirect: function () {
                  return scope.redirect
                },
                serviceName: function () {
                  return scope.serviceName
                },
								redirectParams: function () {
									return scope.redirectParams
								},
								callback: function () {
									return scope.callback
								}
			    		}
			    });
				};
	    }
	  };
	}

	angular
		.module('app')
		.controller('ModalInstanceDeleteDataCtrl', ModalInstanceDeleteDataCtrl);

		ModalInstanceDeleteDataCtrl.$inject = ['$state', '$scope', '$injector', '$uibModalInstance', 'recordId', 'recordPaterId','redirect', 'redirectParams', 'serviceName', '$location', 'toastr', 'callback'];

		function ModalInstanceDeleteDataCtrl ($state, $scope, $injector, $uibModalInstance, recordId, recordPaterId, redirect, redirectParams, serviceName, $location, toastr, callback) {

			$scope.handleDeleteData = function () {

        var Service = $injector.get(serviceName);

				if (recordPaterId) {
					var request = Service.destroy(recordPaterId, recordId)
				} else {
					var request = Service.destroy(recordId);
				}

				return request.success(function (response) {
						let params = redirectParams ? redirectParams : {};
						if (redirect) {
							$state.go(redirect, params)
						}
						if (typeof callback === 'function') {
							callback(response)
						}
						$uibModalInstance.dismiss('cancel');
						toastr.success('Registro eliminado correctamente.');
					}).error(function (response) {
						toastr.error('Registro no se pudo eliminar.')
					});
			}

	  	$scope.cancelModalConsultation = function () {
	    	$uibModalInstance.dismiss('cancel');
	  	};

		}

})();
