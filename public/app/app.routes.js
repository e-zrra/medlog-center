angular
	.module('app')
	.config(["$stateProvider", "$urlRouterProvider", function ($stateProvider, $urlRouterProvider) {
		$stateProvider
			.state('patients', {
				url: '/patients',
				templateUrl: 'app/views/patients/index.html',
				controller: 'PatientCtrl'
			})
			.state('patients-create', {
				url: '/patients/create',
				templateUrl: 'app/views/patients/create.html',
				controller: 'PatientCreateCtrl'
			})
			.state('patients-medical-history', {
				url: '/patients/medical-history/:id',
				templateUrl: 'app/views/patients/medical_history/index.html',
				controller: 'PatientMedicalHistoryCtrl'
			})
			.state('patients-edit', {
				url: '/patients/:id/edit',
				templateUrl: 'app/views/patients/edit.html',
				controller: 'PatientEditCtrl'
			})
			.state('consultations-edit', {
				url: '/consultations/:id/edit',
				templateUrl: 'app/views/consultations/edit.html',
				controller: 'ConsultationEditCtrl'
			})
			.state('profile', {
				url: '/profile',
				templateUrl: 'app/views/profile.html',
				controller: 'ProfileCtrl'
			})
			.state('users', {
				url: '/users',
				templateUrl: 'app/views/users/index.html',
				controller: 'UsersCtrl'
			})
			.state('users-create', {
				url: '/users/create',
				templateUrl: 'app/views/users/create.html',
				controller: 'UserCreateCtrl'
			})
			.state('users-edit', {
				url: '/users/:id/edit',
				templateUrl: 'app/views/users/edit.html',
				controller: 'UserEditCtrl'
			})
			.state('agenda', {
				url: '/agenda',
				templateUrl: 'app/views/agenda/index.html',
				controller: 'AgendaCtrl'
			})
			.state('agenda-create', {
				url: '/agenda/create',
				templateUrl: 'app/views/agenda/create.html',
				controller: 'AgendaCreateCtrl'
			})
			.state('agenda-edit', {
				url: '/agenda/:id/edit',
				templateUrl: 'app/views/agenda/edit.html',
				controller: 'AgendaEditCtrl'
			})

		$urlRouterProvider.otherwise('patients');
	}]);

angular
	.module('app')
	.config(['$httpProvider', 'toastrConfig', function ($httpProvider, toastrConfig) {
		$httpProvider.interceptors.push('TokenInterceptor');

		angular.extend(toastrConfig, {
	    autoDismiss: false,
	    containerId: 'toast-container',
	    maxOpened: 0,
			xtendedTimeOut: 5000,
	    newestOnTop: true,
	    positionClass: 'toast-top-center',
	    preventDuplicates: false,
	    preventOpenDuplicates: false,
	    target: 'body'
	  });

	}])
