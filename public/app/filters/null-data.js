(function () {
'use strict';
	angular
		.module('app')
		.filter('nullData', nullData)

	function nullData () {
		return function (data) {
			return (data === '' || data === null || data === undefined) ? '---' : data;
		};
	}

})();
