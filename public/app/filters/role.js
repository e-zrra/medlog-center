(function () {
'use strict';
	angular
		.module('app')
		.filter('role', role)

	function role () {
		return function (role) {
			if (role === 'admin') return 'Administrador';
			if (role === 'user') return 'Usuario';
			if (role === '' || role === null || role == undefined) return 'None';
		};
	}

})();
