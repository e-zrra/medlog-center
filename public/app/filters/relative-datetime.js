(function () {
'use strict';
	angular
		.module('app')
		.filter('relativeFullDate', relativeFullDate)
		.filter('relativeDate', relativeDate)
		.filter('relativeTime', relativeTime)
		.filter('relativeTimeHourMinute', relativeTimeHourMinute)
		.filter('ago', ago)

    function relativeFullDate () {
  		return function (date) {
  			if (date) return moment(date).format("D MMMM YYYY, h:mm:ss a");
  		}
  	}

		function relativeDate () {
  		return function (date) {
  			if (date) return moment(date).format("D MMMM YYYY");
  		}
  	}

		function relativeTime () {
  		return function (date) {
  			if (date) return moment(date).format("h:mm:ss a");
  		}
  	}

		function relativeTimeHourMinute () {
  		return function (date) {
  			if (date) return moment(date).format("h:mm a");
  		}
  	}

		function ago () {
  		return function (date) {
				var now = moment(new Date());
				var date = moment(date)
  			if (date) return now.from(date, true)
  		}
  	}

})();
