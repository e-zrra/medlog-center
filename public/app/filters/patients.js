(function () {
'use strict';
	angular
		.module('app')
		.filter('gender', gender)
		.filter('age', age);

	function gender () {
		return function (gender) {
			if (gender == 'male') return 'Hombre';
			if (gender == 'female') return 'Mujer';
			if (gender == '' || gender == null) return 'None';
		};
	}

	function age () {
		return function (date) {
			if (date) return moment().diff(date, 'years') + ' años';
			return 'Date not valid';
		};
	}

})();
