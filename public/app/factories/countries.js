(function () {
'use strict';
	angular
		.module('app')
		.factory('Country', country);

	country.$inject = ['$http', '$q'];

	function country ($http, $q) {

		return {
			all: all
		};

		function all() {
			var defer = $q.defer();
			$http.get('/api/countries').then(function (response) {
					defer.resolve(response.data);
				}, function (response) {
					defer.reject('Failed to get data. ' + response);
				});
			return defer.promise;
		};

	}
})();
