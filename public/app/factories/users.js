(function () {
'use strict';
	angular
		.module('app')
		.factory('User', user);

	user.$inject = ['$http', '$q'];

	function user ($http, $q) {

		return {
			index: index,
			create: create,
			get: get,
			update: update,
			destroy: destroy
		};

		function index (currentPage, search) {
			var defer = $q.defer();
			var data = { page: currentPage, search: search };
			$http.get('/api/users', { params: data }).then(function (response) {
					defer.resolve(response.data);
				}, function (response) {
					defer.reject('Failed to get data. ' + response);
				});
			return defer.promise;
		};

		function create (patient) {
			return $http.post('/api/users', patient);
		};

		function get (id) {
			var defer = $q.defer();
			$http.get('/api/users/' + id)
				.then(function (response) {
					defer.resolve(response.data);
				}, function () {
					defer.reject('Failed to get data');
				});
			return defer.promise;
		}

		function update (id, data) {
			return $http.put('/api/users/' + id, data);
		};

		function destroy (id) {
			return $http.delete('/api/users/' + id);
		}

	}
})();
