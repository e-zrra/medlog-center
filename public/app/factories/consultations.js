(function () {
'use strict';
	angular
		.module('app')
		.factory('Consultation', consultation);

	consultation.$inject = ['$http', '$q'];

	function consultation ($http, $q) {

		return {
			get: get,
			create: create,
			update: update,
			destroy: destroy,
			consultationsByPatient: consultationsByPatient
		}

		function create (title, patient_id) {
			var data = { title: title, patient: patient_id };
			return $http.post('/api/consultations', data);
		};

		function get (id) {
			var defer = $q.defer();
			$http.get('/api/consultations/' + id).then(function (response) {
					defer.resolve(response.data);
				}, function (response) {
					defer.reject('Failed to get data');
				});

			return defer.promise;
		};

		function consultationsByPatient (id, currentPage) {
			var defer = $q.defer();
			var data = { page: currentPage };

			$http.get('/api/consultations/patient/' + id, { params: data })
				.then(function (response) {
					defer.resolve(response.data);
				}, function () {
					defer.reject('Failed to get data');
				});

			return defer.promise;
		};

		function update (id, data) {
			return $http.put('/api/consultations/' + id, data);
		};

		function destroy (id) {
			return $http.delete('/api/consultations/' + id);
		}
	};

})();
