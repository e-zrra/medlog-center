(function () {
'use strict';
	angular
		.module('app')
		.factory('File', file);

	file.$inject = ['$http', '$q'];

	function file ($http, $q) {

		return {
			destroy: destroy,
		};

		function destroy (paterId, id) {
			return $http.delete('/api/patients/' + paterId + '/files/' + id);
		}

	}
})();
