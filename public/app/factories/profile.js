(function () {
'use strict';
	angular
		.module('app')
		.factory('Profile', profile);

	profile.$inject = ['$http', '$q'];

	function profile ($http, $q) {

		return {
			get: get,
			update: update
		};

		function get() {
			var defer = $q.defer();
			$http.get('/api/profile')
				.then(function (response) {
					defer.resolve(response.data);
				}, function () {
					defer.reject('Failed to get data');
				});
			return defer.promise;
		}

		function update (data) {
			return $http.put('/api/profile/', data);
		};

	}
})();
