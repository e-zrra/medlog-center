(function () {
'use strict';
	angular
		.module('app')
		.factory('Agenda', agenda);

	agenda.$inject = ['$http', '$q'];

	function agenda ($http, $q) {

		return {
      index: index,
			create: create,
			update: update,
			get: get,
			search: search,
			destroy: destroy,
		};

    function index (currentPage, search) {
			var defer = $q.defer();
			var data = { page: currentPage, search: search };
			$http.get('/api/agenda', { params: data }).then(function (response) {
					defer.resolve(response.data);
				}, function (response) {
					defer.reject('Failed to get data. ' + response);
				});
			return defer.promise;
		};

    function create (appointment) {
			return $http.post('/api/agenda', appointment);
		};

		function search (search) {
			var defer = $q.defer();
			var data = { search: search };
			$http.get('/api/patients/search', { params: data }).then(function (response) {
					defer.resolve(response.data);
				}, function (response) {
					defer.reject('Failed to get data. ' + response);
				});
			return defer.promise;
		};

		function get (id) {
			var defer = $q.defer();
			$http.get('/api/agenda/' + id)
				.then(function (response) {
					defer.resolve(response.data);
				}, function () {
					defer.reject('Failed to get data');
				});
			return defer.promise;
		}

		function update (id, data) {
			return $http.put('/api/agenda/' + id, data);
		};

		function destroy (id) {
			return $http.delete('/api/agenda/' + id);
		}

	}
})();
