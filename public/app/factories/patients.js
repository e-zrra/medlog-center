(function () {
'use strict';
	angular
		.module('app')
		.factory('Patient', patient);

	patient.$inject = ['$http', '$q'];

	function patient ($http, $q) {

		return {
			index: index,
			all: all,
			create: create,
			get: get,
			update: update,
			destroy: destroy,
			updateMedicalHistory: updateMedicalHistory,
			agenda: agenda
		};

		function index (currentPage, search) {
			var defer = $q.defer();
			var data = { page: currentPage, search: search };
			$http.get('/api/patients', { params: data }).then(function (response) {
					defer.resolve(response.data);
				}, function (response) {
					defer.reject('Failed to get data. ' + response);
				});
			return defer.promise;
		};

		function all () {
			var defer = $q.defer();
			$http.get('/api/patients-all').then(function (response) {
					defer.resolve(response.data);
				}, function (response) {
					defer.reject('Failed to get data. ' + response);
				});
			return defer.promise;
		};

		function create (patient) {
			return $http.post('/api/patients', patient);
		};

		function get (id) {
			var defer = $q.defer();
			$http.get('/api/patients/' + id)
				.then(function (response) {
					defer.resolve(response.data);
				}, function () {
					defer.reject('Failed to get data');
				});
			return defer.promise;
		}

		function update (id, data) {
			return $http.put('/api/patients/' + id, data);
		};

		function destroy (id) {
			return $http.delete('/api/patients/' + id);
		}

		function updateMedicalHistory (id, data) {
			return $http.put('/api/patients/' + id + '/medical-history', data);
		};

		function agenda (patient_id) {
			var defer = $q.defer();
			$http.get('/api/patients/' + patient_id + '/agenda')
				.then(function (response) {
					defer.resolve(response.data);
				}, function () {
					defer.reject('Failed to get data');
				});
			return defer.promise;
		}

	}
})();
